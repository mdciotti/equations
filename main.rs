use std::env;
use std::fmt;
use std::convert::From;
use std::str::FromStr;
use std::num::ParseFloatError;
use std::num::ParseIntError;
use std::error::Error;

fn main() {
  let args: Vec<String> = env::args().collect();
  // let config = parse_config(&args);

  if args.len() == 0 {
    println!("Pass a calcuation as command line parameters");
    return;
  }

  let input = &args[1];
  let tokens = Token::tokenize(input);
  let tokens2 = tokens.iter().collect();

  println!("{:?}", tokens);

  let rpn = shunting_yard(tokens2);
  println!("{:?}", rpn);

  println!("{:?}", execute(rpn));
}

#[derive(Debug)]
enum Associativity {
  Left,
  Right,
}

#[derive(Debug)]
enum BinaryOperatorToken {
  Plus,
  Minus,
  Multiply,
  Divide,
}

impl BinaryOperatorToken {
  fn eval(&self, op1: f64, op2: f64) -> f64 {
    match self {
      BinaryOperatorToken::Plus => op1 + op2,
      BinaryOperatorToken::Minus => op1 - op2,
      BinaryOperatorToken::Multiply => op1 * op2,
      BinaryOperatorToken::Divide => op1 / op2,
      // BinaryOperatorToken::Power => op1 op2,
    }
  }
}

#[derive(Debug)]
struct BinaryOperator {
  op: BinaryOperatorToken,
  precedence: i32,
  associativity: Associativity,
}

impl BinaryOperator {
  fn new(op: BinaryOperatorToken, precedence: i32, associativity: Associativity) -> BinaryOperator {
    BinaryOperator { op, precedence, associativity }
  }
}

impl From<&str> for BinaryOperator {
  fn from(s: &str) -> Self {
    match s {
      "+" => BinaryOperator::new(BinaryOperatorToken::Plus, 2, Associativity::Left),
      "-" => BinaryOperator::new(BinaryOperatorToken::Minus, 2, Associativity::Left),
      "*" => BinaryOperator::new(BinaryOperatorToken::Multiply, 3, Associativity::Left),
      "/" => BinaryOperator::new(BinaryOperatorToken::Divide, 3, Associativity::Left),
      // "^" => BinaryOperator::new(BinaryOperatorToken::Power, 4, Associativity::Right),
      _ => panic!("unknown binary operator"),
    }
  }
}

#[derive(Debug)]
enum Token {
  Number(f64),
  Constant(String, f64),
  Variable(String, usize),
  Function(String),
  Operator(BinaryOperator),
  LeftParen,
  RightParen,
}

impl Token {
  fn tokenize(data: &String) -> Vec<Token> {
    data.split(' ')
      .map(|item| item.parse().unwrap())
      // .map(Token::from)
      .collect()
  }
}

impl FromStr for Token {
  type Err = ParseFloatError;

  fn from_str(s: &str) -> Result<Self, Self::Err> {
    match s {
      "(" => Ok(Token::LeftParen),
      ")" => Ok(Token::RightParen),
      "+" | "-" | "*" | "/" => Ok(Token::Operator(BinaryOperator::from(s))),
      "sin" => Ok(Token::Function(String::from(s))),
      &_ if !s.parse::<f64>().is_err() => Ok(Token::Number(s.parse::<f64>()?)),
      &_ => {
        // Ok(Token { value: String::from(s) })
        // Ok(Token::Number(s.parse::<f64>()?))
        Ok(Token::Variable(String::from(s), 0))
      },
    }
  }
}

fn shunting_yard<'a>(tokens: Vec<&'a Token>) -> Vec<&'a Token> {
  let mut out: Vec<&Token> = Vec::new();
  let mut operators: Vec<&Token> = Vec::new();

  for token in tokens.iter() {
    match token {
      Token::Function(_name) => operators.push(token),

      Token::Operator(op1) => {
        loop {
          match operators.last() {
            None => break,
            Some(Token::LeftParen) => break,
            Some(Token::Function(_)) => out.push(operators.pop().unwrap()),
            Some(Token::Operator(op2)) if op2.precedence > op1.precedence => {
              out.push(operators.pop().unwrap())
            },
            Some(Token::Operator(op2)) if op2.precedence == op1.precedence => {
              if let Associativity::Left = op2.associativity {
                out.push(operators.pop().unwrap())
              } else { break }
            },
            // Not a binary operator
            _ => break,
          }
        }
        operators.push(token);
      },

      Token::LeftParen => operators.push(token),

      Token::RightParen => {
        loop {
          match operators.last() {
            None => break,
            Some(Token::LeftParen) => break,
            _ => {
              if let Some(op) = operators.pop() {
                out.push(op);
              } else {
                panic!("mismatched parentheses");
              }
            }
          }
        }
        if let Some(top) = operators.last() {
          if let Token::LeftParen = top {
            operators.pop();
          }
        }
      },

      // Token::Number(n) => out.push(token),
      // Token::Variable(name, reg) => out.push(token),
      // Token::Constant(name, n) => out.push(token),
      _ => out.push(token),
    }
  }

  loop {
    match operators.last() {
      None => break,
      Some(Token::LeftParen) | Some(Token::RightParen) => {
        panic!("mismatched parentheses")
      },
      _ => out.push(operators.pop().unwrap()),
    }
  }

  out
}

fn execute(expr: Vec<&Token>) -> f64 {
  let mut operand_1: f64;
  let mut operand_2: f64;
  let mut stack: Vec<f64> = Vec::new();

  for token in expr {
    match token {
      Token::Operator(op) => {
        operand_2 = stack.pop().unwrap();
        operand_1 = stack.pop().unwrap();
        // stack.push(evaluator(token, operand_1, operand_2))
        stack.push(op.op.eval(operand_1, operand_2))
      },
      Token::Function(_) => (),
      Token::Number(n) => stack.push(*n),
      _ => (),
    }
  }

  match stack.pop() {
    None => panic!("cannot pop empty stack"),
    Some(token) => token,
  }
}