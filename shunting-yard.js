// const $input = document.getElementById('input');
// const $output = document.getElementById('output');

// $input.addEventListener('input', () => {
//   const tokens = tokenize($input.value);
//   const output = shuntingYard(tokens);
//   $output.value = output.join(',');
// });

prompt('Enter a calculation: ', respond);

function respond(input) {
  const tokens = Token.tokenize(input);
  const output = shuntingYard(tokens);
  console.log('rpn:', output.join(' '));
  console.log('infix:', execute(output, infix));
  console.log('prefix:', execute(output, prefix));
  console.log('ast:', execute(output, ast));
  console.log('result:', execute(output, evaluate));
  prompt('Enter a calculation: ', respond);
}

function prompt(question, callback) {
  process.stdin.resume();
  process.stdout.write(question);
  process.stdin.once('data', data => callback(data.toString().trim()));
}

function evaluate(operator, op1, op2) {
  if (operator instanceof FunctionToken) {
    return operator.evaluate(op1, op2);
  } else if (operator instanceof BinaryOperatorToken) {
    return operator.evaluate(op1, op2);
  }
  return null;
}

function ast(operator, ...operands) {
  // return { operator, operand_1, operand_2 };
  return [ operator, ...operands ];
  // return { op: operator, 0: operand_1, 1: operand_2 };
}

function infix(operator, operand_1, operand_2) {
  return operator instanceof FunctionToken
    ? prefix(operator, operand_1, operand_2)
    : `(${operand_1} ${operator} ${operand_2})`;
}

function prefix(operator, ...operands) {
  return `${operator}(${operands.join(', ')})`;
}

class Token {
  /**
   * @param {string} str
   */
  constructor(str) {
    this.value = str;
  }

  toString() { return this.value; }

  /**
   * @param {string} str
   */
  static fromText(str) {
    if (FunctionToken.TOKENS.has(str.toLowerCase())) return new FunctionToken(str);
    if (BinaryOperatorToken.TOKENS.has(str)) return new BinaryOperatorToken(str);

    const constants = new Set(['pi', 'e']);
    if (/-?\d+(?:\.\d+)?/.test(str)) {
      const num = Number.parseFloat(str, 10);
      if (!Number.isNaN(num)) return new NumberToken(str);
    } else if (constants.has(str)) return new NumberToken(str);

    if (/\[R\d+\]/.test(str)) return new VariableToken(str);

    if (str === '(') return new LeftParenthesisToken(str);
    if (str === ')') return new RightParenthesisToken(str);

    return new Token(str);
  }

  /**
   * @param {string} str
   */
  static tokenize(str) {
    const text = str.replace(/\s+/g, ' ').trim();
    return text.split(' ').map(Token.fromText);
    // return text.split(/\b/);
  }
}

class LeftParenthesisToken extends Token {}
class RightParenthesisToken extends Token {}

class FunctionToken extends Token {
  static get TOKENS() {
    return new Map([
      ['max', { arity: 2, fn: Math.max }],
      ['min', { arity: 2, fn: Math.min }],
      ['avg', { arity: 2, fn: (op1, op2) => (op1 + op2) / 2 }],
      ['sin', { arity: 1, fn: Math.sin }],
      ['cos', { arity: 1, fn: Math.cos }],
      ['tan', { arity: 1, fn: Math.tan }],
      ['sqrt', { arity: 1, fn: Math.sqrt }],
    ]);
  }

  get arity() { return FunctionToken.TOKENS.get(this.value.toLowerCase()).arity; }

  evaluate(...operands) {
    const name = this.value.toLowerCase();
    if (FunctionToken.TOKENS.has(name)) {
      return FunctionToken.TOKENS.get(name).fn(...operands);
    } else return Number.NaN;
  }
}

class BinaryOperatorToken extends Token {
  static get TOKENS() {
    return new Map([
      ['+', { precedence: 2, associativity: 'left', fn: (op1, op2) => op1 + op2 }],
      ['-', { precedence: 2, associativity: 'left', fn: (op1, op2) => op1 - op2 }],
      ['*', { precedence: 3, associativity: 'left', fn: (op1, op2) => (op1 + op2) / 2 }],
      ['/', { precedence: 3, associativity: 'left', fn: (op1, op2) => op1 / op2 }],
      ['^', { precedence: 4, associativity: 'right', fn: (op1, op2) => op1 ** op2 }],
    ]);
  }

  get precedence() { return BinaryOperatorToken.TOKENS.get(this.value).precedence; }
  get associativity() { return BinaryOperatorToken.TOKENS.get(this.value).associativity; }

  evaluate(...operands) {
    if (BinaryOperatorToken.TOKENS.has(this.value)) {
      return BinaryOperatorToken.TOKENS.get(this.value).fn(...operands);
    } else return Number.NaN;
  }
}

class NumberToken extends Token {
  constructor(str) {
    super(str);
  }

  valueOf() {
    switch (this.value.toLowerCase()) {
      case 'pi': return Math.PI;
      case 'e': return Math.E;
      default: return Number.parseFloat(this.value);
    }
  }
}

class VariableToken extends Token {

  static get registers() {
    return new Array(10).fill(0);
  }

  constructor(str) {
    super(str);
    this.register = Number.parseInt(str.slice(2, -1));
    console.log(str, this.register);
  }

  toString() {
    return `[R${this.register}]`;
  }

  valueOf() {
    return VariableToken.registers[this.register];
  }
}

/**
 * @param {Array<T>} stack
 * @template T
 */
function getTop(stack) {
  const top = stack[stack.length - 1];
  return typeof top === 'undefined' ? null : top;
}

/**
 * @param {Token[]} tokens
 */
function shuntingYard(tokens) {
  /** @type {Token[]} */
  const output = [];
  /** @type {Token[]} */
  const operators = [];

  while (tokens.length > 0) {
    const token = tokens.shift();
    if (token instanceof NumberToken || token instanceof VariableToken) {
      output.push(token);
    } else if (token instanceof FunctionToken) {
      operators.push(token);
    } else if (token instanceof BinaryOperatorToken) {
      while (getTop(operators) !== null && (getTop(operators) instanceof FunctionToken
        || getTop(operators).precedence > token.precedence
        || getTop(operators).precedence === token.precedence && getTop(operators).associativity === 'left')
        && !(getTop(operators) instanceof LeftParenthesisToken)) {
        output.push(operators.pop());
      }
      operators.push(token);
    } else if (token instanceof LeftParenthesisToken) {
      operators.push(token);
    } else if (token instanceof RightParenthesisToken) {
      while (!(getTop(operators) instanceof LeftParenthesisToken)) {
        if (operators.length === 0) throw new Error('mismatched parentheses');
        output.push(operators.pop());
      }
      if (getTop(operators) instanceof LeftParenthesisToken) {
        operators.pop();
      }
    }
  }

  if (tokens.length === 0) {
    while (operators.length > 0) {
      const top = getTop(operators);
      if (top instanceof LeftParenthesisToken || top instanceof RightParenthesisToken) {
        throw new Error('mismatched parentheses');
      }
      output.push(operators.pop());
    }
  }

  return output;
}

function execute(expression, evaluator = evaluate) {
  let operand_1;
  let operand_2;
  const stack = [];

  for (const token of expression) {
    if (token instanceof BinaryOperatorToken) {
      operand_2 = stack.pop();
      operand_1 = stack.pop();
      stack.push(evaluator(token, operand_1, operand_2));
    } else if (token instanceof FunctionToken) {
      const operands = [];
      while (operands.length < token.arity) operands.unshift(stack.pop());
      stack.push(evaluator(token, ...operands));
    } else if (token instanceof NumberToken) {
      stack.push(token);
    } else {
      stack.push(token);
    }
  }

  return stack.pop();
}
